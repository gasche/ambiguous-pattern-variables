(* font.el *)
type 'a exp =
  | Const of 'a
  | Mul of 'a exp * 'a exp

let is_neutral n = (n = 1)

let mul a b = match a, b with
  | (Const n, v) | (v, Const n) when is_neutral n -> v
  | a, b -> Mul (a, b)
