
----------------------- REVIEW 1 ---------------------
PAPER: 13
TITLE: Ambiguous pattern variables
AUTHORS: Gabriel Scherer, Luc Maranget and Thomas Réfis

OVERALL EVALUATION: 2 (accept)
REVIEWER'S CONFIDENCE: 3 (medium)

----------- Review -----------
The submission describes a recently fixed usability bug related to
 pattern matching with ambiguous pattern variables. The submission
 first gives a motivating example, it then proceeds to recall pattern
 matrices (a common representation for pattern-matching algorithms)
 and finally describes how pattern matrices can be extended with
 'binding sets' to detect ambiguous pattern variables.

 This is a well-written submission. The two pages constitute a dense,
 but delightful and self-contained story on pattern-matching that
 spans from usability to matrix splitting. As such, this reviewer thus
 found his "decadely fix of pattern-matching theory" :-)

 On a more serious note, communicating past lessons of pattern
 matching theory (and extending it) to the next generation of ML
 researchers should not be underestimated. So for more than one reason
 this would be a nice contribution to the ML workshop.


 Detailed comments:

 p.1:
    of the form Some n
 -->of the form Const n


 p.1 This beginning quote `` is missing an end:
    ``warn on ...


 p.2 The example matrix split has some typos:
     q_1
  -->q_{1,1}

     q_{2_1} q_{2,2} p_{1,2} \cdots p_{1,n}
  -->q_{2,1} q_{2,2} p_{2,2} \cdots p_{1,n}   (, instead of _ and 2,2 instead of 1,2)

     q_{4_1}
  -->q_{4,1}


----------------------- REVIEW 2 ---------------------
PAPER: 13
TITLE: Ambiguous pattern variables
AUTHORS: Gabriel Scherer, Luc Maranget and Thomas Réfis

OVERALL EVALUATION: 2 (accept)
REVIEWER'S CONFIDENCE: 5 (expert)

----------- Review -----------
This paper describes a modification to the traditional approach to compiling
pattern matching that includes or-patterns.  The purpose of the modification
is to classify the bound variables as either stable or unstable, where an
unstable variable is one that may be bound in different ways depending on
how or-patterns are resolved.  Unstable variables are a problem when pattern
matching is extended with conditional patterns ("when" patterns in OCaml).
For example, the value (Const x, Const y) will match the lhs clause of the
or-pattern in

        | ((Const n, a) | (a, Const n) when is_nat n) -> a

This clause will fail when (is_nat x = false) and (is_nat y = true), even
though a user might expect it to succeed.  Since the variable n is unstable
in this example, the compiler can issue a warning message.

The paper describes the basic pattern-matrix based approach to pattern-match
compilation and then augments it with binding sets.  One obvious omission
is the affect of "when" patterns on the compilation process.

An alternative approach to compiling or-patterns is to expand them out in
a pre-pass.  With such an approach, it would be possible to duplicate the
conditions and achieve the "expected" behavior.

        | ((Const n, a) | (a, Const n) when is_nat n) -> a

expands to

        | (Const n, a) when is_nat n -> a'()
        | (a, Const n) when is_nat n -> a'()

where a' = \().a

The result of this paper is minor, but it is a useful revisiting of the
concepts behind pattern-match compilation.  The final version should include
references to the literature, to better serve that purpose.


----------------------- REVIEW 3 ---------------------
PAPER: 13
TITLE: Ambiguous pattern variables
AUTHORS: Gabriel Scherer, Luc Maranget and Thomas Réfis

OVERALL EVALUATION: 2 (accept)
REVIEWER'S CONFIDENCE: 4 (high)

----------- Review -----------
This paper points out an issue related to the usage of or-patterns
with a guard, the behavior of which may be different from users'
intension.  This paper formulates this mismatch by introducing
"ambiguity" (or stability) of pattern variables and presents a
strategy to detect ambiguous pattern variables.  This paper also
says that OCaml 4.03 warns this kinds of problematic usage of
or-patterns (probably) based on this idea.

I truly enjoyed to read this paper.  The issue concerned in this
paper is interesting.  The workaround for this issue this paper
presents does not seem elegant, but pragmatic.  This issue and
workaround should be shared among all ML compiler developers as
well as OCaml community; therefore, I think this is worthy of
presentation at ML workshop.

While the main topic of this paper is interesting, the latter half
of this paper should need improvement.  Because of too brief
description of pattern matrices, I suspect that readers who do not
know about them cannot follow this paper, whereas the issue is
easy to understand and the workaround is not so complicated.
I strongly suggest to review and rewrite the sections after
"Pattern matrices" and add appropriate references to further
readings about pattern match compilation for the readers who don't
know match compile very much but want to understand its details.

Some comments for further improvements:

* This paper says that OCaml 4.03 warns this kind of issues, but
does not claim that this warning is introduced by this work.  If
OCaml compiler prints warnings based on the approach presented in
this paper, you should mention this fact explicitly.

* I guess the discussion after "Pattern matrices" section is based
on the analysis on pattern matching reported by Leroy in his ZINC
report.  If so, you should add a reference to it.

* Describe why the example of pattern matrices with K1, K2, and a
"don't care" pattern is splitted into three sub-matrices and what
they denote.

* The three submatrices have unique brackets.  Is there any meaning
in the usage of brackets?

* typos in subscripts:
p_{3,1} -> p_{3,2}
q1 -> q_{1,1}
q_{2_1} -> q_{2,1}
q_{4_1} -> q_{4,1}

* In "Bindings": it is not clear how l and B_{m,l} are maintained.
What is "potision" in the description of binding sets?
Is there any correspondence between B_{i,j} and p_{i,j}?
How are l and B_{m,l} initialized?
